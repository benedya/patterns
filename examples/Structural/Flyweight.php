<?php

include __DIR__ . "/../../vendor/autoload.php";

$teaMaker = new \Benedya\Patterns\Structural\Flyweight\TeaMaker();
$teaMarket = new \Benedya\Patterns\Structural\Flyweight\TeaMarket($teaMaker);

$teaMarket->takeOrder('green', 7);
$teaMarket->takeOrder('green duble', 3);
$teaMarket->takeOrder('green', 4);
$teaMarket->takeOrder('green', 4);

$teaMarket->serve();

echo "\n created objects " . $teaMaker->count();
