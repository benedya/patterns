<?php

include __DIR__ . "/../../vendor/autoload.php";

$form = new \Benedya\Patterns\Structural\Composite\Form();
$form->addElement(
    new \Benedya\Patterns\Structural\Composite\Input('Login')
);
$form->addElement(
    new \Benedya\Patterns\Structural\Composite\Password()
);

$subForm = new \Benedya\Patterns\Structural\Composite\Form();
$subForm->addElement(
    new \Benedya\Patterns\Structural\Composite\Input('Name')
);
$subForm->addElement(
    new \Benedya\Patterns\Structural\Composite\Input('Surname')
);
$form->addElement($subForm);

echo '' . $form->render();
