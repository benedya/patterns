<?php

include __DIR__ . "/../../vendor/autoload.php";


$about = new \Benedya\Patterns\Structural\Bridge\About(
    new \Benedya\Patterns\Structural\Bridge\DarkTheme()
);
$career = new \Benedya\Patterns\Structural\Bridge\Career(
    new \Benedya\Patterns\Structural\Bridge\AquaTheme()
);

echo "\n " . $about->getContent();
echo "\n " . $career->getContent();
