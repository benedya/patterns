<?php

include __DIR__ . "/../../vendor/autoload.php";

$hunter = new \Benedya\Patterns\Structural\Adapter\Hunter();
$africanLion = new \Benedya\Patterns\Structural\Adapter\AfricanLion();
$asianLion = new \Benedya\Patterns\Structural\Adapter\AsianLion();
$wildDog = new \Benedya\Patterns\Structural\Adapter\WildDogAdapter(
    new \Benedya\Patterns\Structural\Adapter\WildDog()
);

$hunter->hunt($africanLion);
$hunter->hunt($asianLion);
$hunter->hunt($wildDog);
