<?php

include __DIR__ . "/../../vendor/autoload.php";

$configuration = new \Benedya\Patterns\Structural\DependencyInjection\DatabaseConfiguration(
    'localhost',
    'root',
    'db',
    'turbo'
);
$connection = new \Benedya\Patterns\Structural\DependencyInjection\DatabaseConnection($configuration);

var_dump($connection->getDsn());
