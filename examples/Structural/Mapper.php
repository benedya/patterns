<?php

include __DIR__ . "/../../vendor/autoload.php";

$adapter = new \Benedya\Patterns\Structural\Mapper\Adapter([
    1 => ['name' => 'Bohdan', 'email' => 'benedya@gmail.com']
]);

$mapper = new \Benedya\Patterns\Structural\Mapper\UserMapper($adapter);

print_r($mapper->findById(1));
