<?php

include __DIR__ . "/../../vendor/autoload.php";

$coffee = new \Benedya\Patterns\Structural\Decorator\SimpleCoffee(10);

echo sprintf("\n %s costs %s", $coffee->getDescription(), $coffee->getCost());

$milkCoffeeDecorator = new \Benedya\Patterns\Structural\Decorator\MilkCoffeeDecorator(
    $coffee
);

echo sprintf("\n %s %s costs", $milkCoffeeDecorator->getDescription(), $milkCoffeeDecorator->getCost());
