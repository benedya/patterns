<?php

include __DIR__ . "/../../vendor/autoload.php";

$facade = new \Benedya\Patterns\Structural\Facade\Facade(
    new \Benedya\Patterns\Structural\Facade\Computer()
);
$facade->on();
$facade->off();
