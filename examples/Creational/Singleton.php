<?php

include __DIR__ . "/../../vendor/autoload.php";

$singleton = \Benedya\Patterns\Creational\Singelton\President::getInstance();
$singleton2 = \Benedya\Patterns\Creational\Singelton\President::getInstance();
print_r($singleton == $singleton2);
