<?php

include __DIR__ . "/../../vendor/autoload.php";

$factory = new \Benedya\Patterns\Creational\AbstractFactory\WoodFactory();
echo "\n Wood factory";
echo "\n Door: " . $factory->makeDoor()->getDescription();
echo "\n Expert: " . $factory->makeExpert()->getDescription();

echo "\n\n\n";

$factory = new \Benedya\Patterns\Creational\AbstractFactory\IronFactory();
echo "\n Iron factory";
echo "\n Door: " . $factory->makeDoor()->getDescription();
echo "\n Expert: " . $factory->makeExpert()->getDescription();
