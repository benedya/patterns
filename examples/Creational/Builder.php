<?php

include __DIR__ . "/../../vendor/autoload.php";

$builder = new \Benedya\Patterns\Creational\Builder\BurgerBuilder(10);
$burger = new \Benedya\Patterns\Creational\Builder\Burger(
    $builder
        ->addCheese()
);
print_r($burger);
$builder->addMeat();
print_r($builder->buildBurger());
