<?php

include __DIR__ . "/../../vendor/autoload.php";

echo "\n ///////////// marketer interviewer";
$interviewer = new \Benedya\Patterns\Creational\FabricMethod\MarketerInterviewer();
$interviewer->takeInterview();

echo "\n //////////// developer interviewer";
$interviewer = new \Benedya\Patterns\Creational\FabricMethod\DeveloperInterviewer();
$interviewer->takeInterview();

