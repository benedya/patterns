<?php

include __DIR__ . "/../../vendor/autoload.php";

$prototype = new \Benedya\Patterns\Creational\Prototype\Sheep('Sofa', 'Mountain Sheep');
print_r($prototype);
$prototype2 = clone $prototype;
$prototype2->setName('Inga');
print_r($prototype2);
