<?php

include __DIR__ . "/../../vendor/autoload.php";


$originator = (new \Benedya\Patterns\Behavioral\Memento\Originator())
    ->setState('on');

echo sprintf("\n-> state %s", $originator->getState());
echo sprintf("\n -- create caretaker -- \n");

$caretaker = (new \Benedya\Patterns\Behavioral\Memento\Caretaker())
    ->setMemento($originator->createMemento())
;
echo sprintf("\n -- change state -- \n");

$originator->setState('off');

echo sprintf("\n-> state %s", $originator->getState());
echo sprintf("\n -- roll back -- \n");

$originator->setMemento($caretaker->getMemento());

echo sprintf("\n-> state %s", $originator->getState());

