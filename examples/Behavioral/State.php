<?php

include __DIR__ . "/../../vendor/autoload.php";

$context = new Benedya\Patterns\Behavioral\State\ContextOrder();
$created = new \Benedya\Patterns\Behavioral\State\CreatedOrder();

$context->setState($created);
echo "\n -> " . $context->getStatus();
$context->done();
echo "\n -> " . $context->getStatus();
$context->done();
echo "\n -> " . $context->getStatus();
