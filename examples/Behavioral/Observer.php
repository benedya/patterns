<?php

include __DIR__ . "/../../vendor/autoload.php";

$user = new \Benedya\Patterns\Behavioral\Observer\User();

$watcher = new \Benedya\Patterns\Behavioral\Observer\Watcher();
$user->attach($watcher);

$user->setEmail('benedya');

var_dump($watcher->getChanged());
