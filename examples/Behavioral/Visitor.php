<?php

include __DIR__ . "/../../vendor/autoload.php";


$visitor = new Benedya\Patterns\Behavioral\Visitor\Visitor();
$user = new \Benedya\Patterns\Behavioral\Visitor\User('User one');
$group = new \Benedya\Patterns\Behavioral\Visitor\Group('Group one');

$user->acceptVisitor($visitor);
$group->acceptVisitor($visitor);

print_r($visitor->getVisited());
