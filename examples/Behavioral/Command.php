<?php

include __DIR__ . "/../../vendor/autoload.php";

$command = new \Benedya\Patterns\Behavioral\Command\HelloCommand(
    new \Benedya\Patterns\Behavioral\Command\Receiver()
);
$invoker = new \Benedya\Patterns\Behavioral\Command\Invoker($command);
$invoker->run();
