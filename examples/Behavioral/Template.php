<?php

include __DIR__ . "/../../vendor/autoload.php";


$cityJourney = new \Benedya\Patterns\Behavioral\Template\CityJourney();
$seaJourney = new \Benedya\Patterns\Behavioral\Template\SeaJourney();

$cityJourney->doIt();
$seaJourney->doIt();

echo "\n city journey";
print_r($cityJourney->getThingsTodo());

echo "\n sea journey";
print_r($seaJourney->getThingsTodo());
