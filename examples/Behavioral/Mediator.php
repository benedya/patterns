<?php

include __DIR__ . "/../../vendor/autoload.php";

$user = new \Benedya\Patterns\Behavioral\Mediator\User(
    'John',
    new Benedya\Patterns\Behavioral\Mediator\ChatMediator()
);
$user->notify('Hi!');
