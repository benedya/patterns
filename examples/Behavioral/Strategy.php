<?php

include __DIR__ . "/../../vendor/autoload.php";

if (rand(0, 1)) {
    $strategy = new \Benedya\Patterns\Behavioral\Strategy\ZipStrategy();
} else {
    $strategy = new \Benedya\Patterns\Behavioral\Strategy\TarStrategy();
}

$context = new \Benedya\Patterns\Behavioral\Strategy\Context($strategy);

var_dump($context->execute('test'));
