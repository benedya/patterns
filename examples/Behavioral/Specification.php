<?php

include __DIR__ . "/../../vendor/autoload.php";

$priceSpecification = new \Benedya\Patterns\Behavioral\Specification\PriceSpecification(4, 10);
$priceSpecification2 = new \Benedya\Patterns\Behavioral\Specification\PriceSpecification(1, 5);

$andSpecification = new \Benedya\Patterns\Behavioral\Specification\AndSpecification(
    $priceSpecification,
    $priceSpecification2
);


$res = $andSpecification->isSatisfiedBy(new \Benedya\Patterns\Behavioral\Specification\Item(5));
echo "\n result is ";
var_dump($res);
