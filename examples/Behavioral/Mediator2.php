<?php

include __DIR__ . "/../../vendor/autoload.php";

$mediator = new Benedya\Patterns\Behavioral\Mediator2\ChatMediator();

$userOne = new \Benedya\Patterns\Behavioral\Mediator2\UserOne('Bodan', $mediator);
$userTwo = new \Benedya\Patterns\Behavioral\Mediator2\UserTwo('Mariana', $mediator);
$mediator->setUserOne($userOne)->setUserTwo($userTwo);

//$mediator->send($userOne, 'Hey, Mariana');
//$mediator->send($userTwo, 'Hey, Bodan');

$userOne->send('Hi all');
$userTwo->send('Hello');
