<?php

include __DIR__ . "/../../vendor/autoload.php";


$postRepository = new \Benedya\Patterns\Additional\Repository\PostRepository(
    new \Benedya\Patterns\Additional\Repository\MemoryStorage()
);

$post = \Benedya\Patterns\Additional\Repository\Post::fromState([
    'title' => 'Test',
    'description' => 'Test',
]);

$postRepository->save($post);

print_r($postRepository->findById(1));
