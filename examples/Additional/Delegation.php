<?php

include __DIR__ . "/../../vendor/autoload.php";

(new \Benedya\Patterns\Additional\Delegation\LeadDeveloper(
    new \Benedya\Patterns\Additional\Delegation\JuniorDeveloper()
))->writeCode();
