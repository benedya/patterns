<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Observer;

class User implements \SplSubject
{
    protected $email;

    /** @var  \SplObjectStorage */
    protected $observers;

    function __construct()
    {
        $this->observers = new \SplObjectStorage();
    }

    public function attach(\SplObserver $observer)
    {
        $this->observers->attach($observer);
    }

    public function detach (\SplObserver $observer)
    {
        $this->observers->detach($observer);
    }

    public function notify ()
    {
        /** @var \SplObserver $observer */
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }

    /**
     * @param mixed $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        $this->notify();
        return $this;
    }
}
