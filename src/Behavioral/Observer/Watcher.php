<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Observer;

class Watcher implements \SplObserver
{
    protected $changed = [];

    public function update (\SplSubject $subject)
    {
        $this->changed[] = $subject;
    }

    /**
     * @return array
     */
    public function getChanged()
    {
        return $this->changed;
    }
}
