<?php

namespace Benedya\Patterns\Behavioral\Template;

abstract class AbstractJourney
{
    protected $thingsTodo = [];

    public final function doIt()
    {
        $this->thingsTodo = [
            $this->buyGift(),
            $this->buyTicket(),
            $this->enjoy(),
        ];
    }

    protected function buyGift(): string
    {
        return "\n gift bought";
    }

    protected function buyTicket(): string
    {
        return "\n ticket bought";
    }

    protected abstract function enjoy(): string;

    public function getThingsTodo(): array
    {
        return $this->thingsTodo;
    }
}
