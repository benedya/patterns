<?php

namespace Benedya\Patterns\Behavioral\Template;

class SeaJourney extends AbstractJourney
{
    protected function enjoy(): string
    {
        return "\n take a rest on a beach";
    }
}
