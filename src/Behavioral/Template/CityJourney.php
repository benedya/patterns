<?php

namespace Benedya\Patterns\Behavioral\Template;

class CityJourney extends AbstractJourney
{
    protected function enjoy(): string
    {
        return "\n drink, eat, sleep";
    }
}
