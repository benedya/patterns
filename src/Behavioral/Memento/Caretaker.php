<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Memento;

class Caretaker
{
    /** @var  Memento */
    protected $memento;

    public function setMemento(Memento $memento): Caretaker
    {
        $this->memento = $memento;
        return $this;
    }

    public function getMemento(): Memento
    {
        return $this->memento;
    }
}
