<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Memento;

class Originator
{
    protected $state;

    public function setState($state)
    {
        $this->state = $state;
        echo sprintf("\n set state %s", $state);
        return $this;
    }

    public function createMemento()
    {
        return (new Memento($this->state));
    }

    public function setMemento(Memento $memento): void
    {
        $this->state = $memento->getState();
    }

    public function getState(): string
    {
        return $this->state;
    }
}
