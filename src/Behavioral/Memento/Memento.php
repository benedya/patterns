<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Memento;

class Memento
{
    protected $state;

    function __construct(string $state)
    {
        $this->state = $state;
    }

    public function getState(): string
    {
        return $this->state;
    }
}
