<?php

namespace Benedya\Patterns\Behavioral\Strategy;

interface NamingStrategyInterface
{
    function getName($filename);
}
