<?php

namespace Benedya\Patterns\Behavioral\Strategy;

class TarStrategy implements NamingStrategyInterface
{
    function getName($filename)
    {
        return 'http://google.com' . $filename .'.tar';
    }
}
