<?php

namespace Benedya\Patterns\Behavioral\Strategy;

class Context
{
    protected $namingStrategy;

    function __construct(NamingStrategyInterface $namingStrategy)
    {
        $this->namingStrategy = $namingStrategy;
    }

    public function execute($fielname)
    {
        return $this->namingStrategy->getName($fielname);
    }
}
