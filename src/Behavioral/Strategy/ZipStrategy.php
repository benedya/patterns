<?php

namespace Benedya\Patterns\Behavioral\Strategy;

class ZipStrategy implements NamingStrategyInterface
{
    function getName($filename)
    {
        return 'http://google.com' . $filename .'.zip';
    }
}
