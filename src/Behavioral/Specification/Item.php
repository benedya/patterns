<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Specification;

class Item
{
    protected $price;

    function __construct(float $price)
    {
        $this->price = $price;
    }

    public function getPrice(): float
    {
        return $this->price;
    }
}
