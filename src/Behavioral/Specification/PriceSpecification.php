<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Specification;

class PriceSpecification implements SpecificationInterface
{
    protected $min;
    protected $max;

    function __construct(float $min, float $max)
    {
        $this->min = $min;
        $this->max = $max;
    }

    function isSatisfiedBy(Item $item): bool
    {
        if ($item->getPrice() >= $this->min and $item->getPrice() <= $this->max) {
            return true;
        }
        return false;
    }
}
