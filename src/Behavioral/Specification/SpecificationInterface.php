<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Specification;

interface SpecificationInterface
{
    function isSatisfiedBy(Item $item): bool;
}
