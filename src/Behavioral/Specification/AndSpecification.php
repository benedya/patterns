<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Specification;

class AndSpecification implements SpecificationInterface
{
    protected $specifications;

    function __construct(SpecificationInterface ...$specifications)
    {
        $this->specifications = $specifications;
    }

    function isSatisfiedBy(Item $item): bool
    {
        /** @var SpecificationInterface $specification */
        foreach ($this->specifications as $specification) {
            if (!$specification->isSatisfiedBy($item)) {
                return false;
            }
        }
        return true;
    }
}
