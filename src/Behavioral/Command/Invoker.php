<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Command;

class Invoker
{
    protected $command;

    function __construct(CommandInterface $command)
    {
        $this->command = $command;
    }

    public function run()
    {
        $this->command->execute();
    }
}
