<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Command;

class Receiver
{
    public function write(string $string)
    {
        echo "\n > " . $string;
    }
}
