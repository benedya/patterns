<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Command;

class HelloCommand implements CommandInterface
{
    protected $receiver;

    function __construct(Receiver $receiver)
    {
        $this->receiver = $receiver;
    }

    public function execute()
    {
        $this->receiver->write('Hello');
    }
}
