<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Command;

interface CommandInterface
{
    function execute();
}
