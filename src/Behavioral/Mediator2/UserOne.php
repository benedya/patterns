<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Mediator2;

class UserOne extends User
{
    public function receiveMsg(string $msg)
    {
        echo sprintf("%s gets %s \n", $this->name, $msg);
    }
}
