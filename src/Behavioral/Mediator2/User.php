<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Mediator2;

abstract class User
{
    protected $name;
    protected $mediator;

    function __construct($name, MediatorInterface $mediator)
    {
        $this->name = $name;
        $this->mediator = $mediator;
    }

    public function send(string $msg)
    {
        $this->mediator->send($this, $msg);
    }

    public abstract function receiveMsg(string $msg);
}
