<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Mediator2;

class ChatMediator implements MediatorInterface
{
    protected $userOne;
    protected $userTwo;

    function send(User $user, string $msg)
    {
        if ($user == $this->userOne) {
            $this->userTwo->receiveMsg($msg);
        } else {
            $this->userOne->receiveMsg($msg);
        }
    }

    public function setUserOne(User $userOne): ChatMediator
    {
        $this->userOne = $userOne;
        return $this;
    }

    public function setUserTwo(User $userTwo): ChatMediator
    {
        $this->userTwo = $userTwo;
        return $this;
    }
}
