<?php

namespace Benedya\Patterns\Behavioral\Visitor;

interface VisitorInterface
{
    function visitUser(User $user);
    function visitGroup(Group $group);
}
