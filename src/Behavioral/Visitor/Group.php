<?php

namespace Benedya\Patterns\Behavioral\Visitor;

class Group implements RoleInterface
{
    protected $name;

    function __construct(string $name)
    {
        $this->name = $name;
    }

    function acceptVisitor(VisitorInterface $visitor)
    {
        $visitor->visitGroup($this);
    }
}
