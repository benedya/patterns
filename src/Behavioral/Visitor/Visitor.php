<?php

namespace Benedya\Patterns\Behavioral\Visitor;

class Visitor implements VisitorInterface
{
    protected $visited = [];

    function visitUser(User $user)
    {
        $this->visited[] = $user;
    }

    function visitGroup(Group $group)
    {
        $this->visited[] = $group;
    }

    /**
     * @return array
     */
    public function getVisited()
    {
        return $this->visited;
    }
}
