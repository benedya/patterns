<?php

namespace Benedya\Patterns\Behavioral\Visitor;

interface RoleInterface
{
    function acceptVisitor(VisitorInterface $visitor);
}
