<?php

namespace Benedya\Patterns\Behavioral\Visitor;

class User implements RoleInterface
{
    protected $userName;

    function __construct(string $userName)
    {
        $this->userName = $userName;
    }

    function acceptVisitor(VisitorInterface $visitor)
    {
        $visitor->visitUser($this);
    }
}
