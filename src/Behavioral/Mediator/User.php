<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Mediator;

class User
{
    protected $name;
    protected $mediator;

    function __construct($name, MediatorInterface $mediator)
    {
        $this->name = $name;
        $this->mediator = $mediator;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function notify(string $msg)
    {
        $this->mediator->send($this, $msg);
    }
}
