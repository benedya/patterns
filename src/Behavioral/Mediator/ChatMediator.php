<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Mediator;

class ChatMediator implements MediatorInterface
{
    function send(User $user, string $msg)
    {
        echo sprintf("%s [%s] %s\n ", $user->getName(), date('d.m.Y H:i'), $msg);
    }
}
