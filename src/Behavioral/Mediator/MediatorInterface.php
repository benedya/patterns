<?php

declare(strict_types=1);

namespace Benedya\Patterns\Behavioral\Mediator;

interface MediatorInterface
{
    function send(User $user, string $msg);
}
