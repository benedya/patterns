<?php

namespace Benedya\Patterns\Behavioral\State;

class ShippingOrder extends StateOrder
{
    function __construct()
    {
        $this->setStatus('shipping');
    }

    public function done()
    {
        $this->setStatus('completed');
    }
}
