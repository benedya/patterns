<?php

namespace Benedya\Patterns\Behavioral\State;

class ContextOrder extends StateOrder
{
    public function setStatus($status)
    {
        self::$state->setStatus($status);
    }

    public function setState($state)
    {
        self::$state = $state;
    }

    public function getStatus()
    {
        return self::$state->getStatus();
    }

    public function done()
    {
        self::$state->done();
    }
}
