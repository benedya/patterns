<?php

namespace Benedya\Patterns\Behavioral\State;

abstract class StateOrder
{
    /** @var  StateOrder */
    protected static $state;
    protected $details;

    /**
     * @return StateOrder
     */
    public function getState()
    {
        return self::$state;
    }

    /**
     * @param StateOrder $state
     */
    public function setState($state)
    {
        self::$state = $state;
    }

    public function setStatus($status)
    {
        $this->details['status'] = $status;
        $this->details['updTime'] = time();
    }

    public function getStatus()
    {
        return $this->details['status'];
    }

    public abstract function done();
}
