<?php

namespace Benedya\Patterns\Behavioral\State;

class CreatedOrder extends StateOrder
{
    function __construct()
    {
        $this->setStatus('created');
    }

    public function done()
    {
        self::$state = new ShippingOrder();
    }
}
