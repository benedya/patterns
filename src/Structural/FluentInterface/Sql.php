<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\FluentInterface;

class Sql
{
    protected $fields;
    protected $tables;
    protected $conditions;

    public function select(array $fields): Sql
    {
        $this->fields = $fields;
        return $this;
    }

    public function from(string $table, string $alias): Sql
    {
        $this->tables[] = $table .' as ' . $alias;
        return $this;
    }

    public function where(string $condition): Sql
    {
        $this->conditions[] = $condition;
        return $this;
    }

    function __toString(): string
    {
        return sprintf(
            'select %s from %s where %s',
            join(', ', $this->fields),
            join(', ', $this->tables),
            join(' and ', $this->conditions)
        );
    }
}
