<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Flyweight;

class TeaMaker
{
    protected $pool = [];

    public function make(string $type): GreenTea
    {
        if (!isset($this->pool[$type])) {
            $this->pool[$type] = new GreenTea();
        }

        return $this->pool[$type];
    }

    public function count()
    {
        return count($this->pool);
    }
}
