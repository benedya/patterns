<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Flyweight;

class TeaMarket
{
    protected $orders = [];
    protected $maker;

    function __construct(TeaMaker $maker)
    {
        $this->maker = $maker;
    }

    public function takeOrder(string $type, int $table): void
    {
        $this->orders[$table] = $this->maker->make($type);
    }

    public function serve(): void
    {
        /** @var GreenTea $order */
        foreach ($this->orders as $table => $order) {
            echo sprintf("\n table %s type %s", $table, $order->getType());
        }
    }
}
