<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Flyweight;

class GreenTea
{
    protected $type = 'Green';

    public function getType(): string
    {
        return $this->type;
    }
}
