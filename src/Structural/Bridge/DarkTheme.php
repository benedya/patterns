<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Bridge;

class DarkTheme implements ITheme
{
    public function getColor(): string
    {
        return 'dark';
    }
}
