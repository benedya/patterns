<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Bridge;

interface IWebPage
{
    function getContent(): string;
}
