<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Bridge;

interface ITheme
{
    function getColor(): string;
}
