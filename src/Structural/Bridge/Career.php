<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Bridge;

class Career implements IWebPage
{
    protected $theme;

    function __construct(ITheme $theme)
    {
        $this->theme = $theme;
    }

    function getContent(): string
    {
        return sprintf('Career is %s', $this->theme->getColor());
    }
}
