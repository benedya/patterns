<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Bridge;

class AquaTheme implements ITheme
{
    public function getColor(): string
    {
        return 'aqua';
    }
}
