<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\DependencyInjection;

class DatabaseConnection
{
    protected $configuration;

    function __construct(DatabaseConfiguration $configuration)
    {
        $this->configuration = $configuration;
    }

    public function getDsn()
    {
        return sprintf(
            '%s:%s:%s:%s',
            $this->configuration->getHost(),
            $this->configuration->getName(),
            $this->configuration->getUser(),
            $this->configuration->getPassword()
        );
    }
}
