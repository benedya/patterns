<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\DependencyInjection;

class DatabaseConfiguration
{
    protected $host;
    protected $user;
    protected $name;
    protected $password;

    function __construct(string $host, string $user, string $name, string $password)
    {
        $this->host = $host;
        $this->user = $user;
        $this->name = $name;
        $this->password = $password;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getUser(): string
    {
        return $this->user;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
