<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Adapter;

interface ILion
{
    function roar(): void;
}
