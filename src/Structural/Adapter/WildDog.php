<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Adapter;

class WildDog
{
    public function bark(): void
    {
        echo "\n wof";
    }
}
