<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Adapter;

class Hunter
{
    public function hunt(ILion $lion): void
    {
        $lion->roar();
    }
}
