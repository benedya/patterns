<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Adapter;

class WildDogAdapter implements ILion
{
    protected $dog;

    function __construct(WildDog $dog)
    {
        $this->dog = $dog;
    }

    public function roar(): void
    {
        $this->dog->bark();
    }
}
