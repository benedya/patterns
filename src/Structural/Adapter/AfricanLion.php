<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Adapter;

class AfricanLion implements ILion
{
    public function roar(): void
    {
        echo "\n rrr";
    }
}
