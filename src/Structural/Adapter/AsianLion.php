<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Adapter;

class AsianLion implements ILion
{
    public function roar(): void
    {
        echo "\n urr";
    }
}
