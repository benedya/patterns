<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Mapper;

class Adapter
{
    protected $data;

    function __construct(array $data)
    {
        $this->data = $data;
    }

    public function find(int $id)
    {
        if (isset($this->data[$id])) {
            return $this->data[$id];
        }
        return null;
    }
}
