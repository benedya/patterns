<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Mapper;

class UserMapper
{
    protected $adapter;
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function findById(int $id): User
    {
        if (!$row = $this->adapter->find($id)) {
            throw new \Exception(sprintf('User not found by id #%s', $id));
        }

        return $this->mapRowToUser($row);
    }

    protected function mapRowToUser(array $data): User
    {
        return User::create($data);
    }
}
