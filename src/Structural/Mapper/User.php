<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Mapper;

class User
{
    protected $name;
    protected $email;

    function __construct(string $name, string $email)
    {
        $this->name = $name;
        $this->email = $email;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public static function create(array $data): User
    {
        return new self($data['name'], $data['email']);
    }
}
