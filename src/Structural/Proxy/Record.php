<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Proxy;

class Record
{
    protected $data;

    function __construct(array $data)
    {
        $this->data = $data;
    }

    function __set(string $name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __get(string $name)
    {
        if (!isset($this->data[$name])) {
            throw new \Exception('Option not found');
        }
        return $this->data[$name];
    }
}
