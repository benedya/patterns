<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Proxy;

class RecordProxy extends Record
{
    protected $isDirty;
    protected $isInitialized;

    function __construct(array $data)
    {
        parent::__construct($data);
        if (count($data)) {
            $this->isInitialized = true;
            $this->isDirty = true;
        }
    }

    function __set(string $name, $value)
    {
        $this->isDirty = true;
        parent::__set($name, $value);
    }
}
