<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Facade;

class Computer
{
    public function getElectricShock(): void
    {
        echo "\n electric shock";
    }

    public function sayBip(): void
    {
        echo "\n bip";
    }

    public function showLoadingScreen(): void
    {
        echo "\n loading...";
    }

    public function ready(): void
    {
        echo "\n ready";
    }

    public function closeAll(): void
    {
        echo "\n close all";
    }

    public function sooth(): void
    {
        echo "\n zzz";
    }
}
