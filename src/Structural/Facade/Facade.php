<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Facade;

class Facade
{
    protected $computer;

    function __construct(Computer $computer)
    {
        $this->computer = $computer;
    }

    public function on()
    {
        $this->computer->getElectricShock();
        $this->computer->sayBip();
        $this->computer->showLoadingScreen();
        $this->computer->ready();
    }

    public function off()
    {
        $this->computer->closeAll();
        $this->computer->sooth();
    }
}
