<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Decorator;

interface CoffeeInterface
{
    function getCost(): int;
    function getDescription(): string;
}
