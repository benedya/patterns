<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Decorator;

class SimpleCoffee implements CoffeeInterface
{
    protected $cost;

    function __construct(int $cost)
    {
        $this->cost = $cost;
    }

    public function getCost(): int
    {
        return $this->cost;
    }

    function getDescription(): string
    {
        return 'Coffee';
    }
}
