<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Decorator;

class MilkCoffeeDecorator implements CoffeeInterface
{
    protected $coffee;

    function __construct(CoffeeInterface $coffee)
    {
        $this->coffee = $coffee;
    }

    public function getCost(): int
    {
        return $this->coffee->getCost() + 2;
    }

    function getDescription(): string
    {
        return $this->coffee->getDescription() . ', milk';
    }
}
