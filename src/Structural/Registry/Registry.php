<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Registry;

class Registry
{
    const LOGGER = 'logger';

    static $values = [];

    static $acceptedKeys = [self::LOGGER];

    public static function set(string $key, $value)
    {
        if (!in_array($key, self::$acceptedKeys)) {
            throw new \InvalidArgumentException('Invalid key.');
        }
        self::$values[$key] = $value;
    }

    public static function get(string $key)
    {
        if (!isset(self::$values[$key])) {
            throw new \InvalidArgumentException('Invalid key.');
        }
        return self::$values[$key];
    }
}
