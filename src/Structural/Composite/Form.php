<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Composite;

class Form implements FormInterface
{
    /** @var FormInterface [] */
    protected $elements = [];

    function addElement(FormInterface $el)
    {
        $this->elements[] = $el;
    }

    public function render(): string
    {
        $str = '<form>';
        foreach ($this->elements as $el) {
            $str .= $el->render();
        }
        $str .= '</form>';

        return $str;
    }
}
