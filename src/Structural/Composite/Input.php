<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Composite;

class Input implements FormInterface
{
    protected $label;

    function __construct(string $label)
    {
        $this->label = $label;
    }

    public function render(): string
    {
        return sprintf('<label>%s</label><input type="text">', $this->label);
    }
}
