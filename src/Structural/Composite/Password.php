<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Composite;

class Password implements FormInterface
{
    public function render(): string
    {
        return sprintf('<label>Password</label><input type="password">');
    }
}
