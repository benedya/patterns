<?php

declare(strict_types=1);

namespace Benedya\Patterns\Structural\Composite;

interface FormInterface
{
    function render(): string;
}
