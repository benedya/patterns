<?php

declare(strict_types=1);

namespace Benedya\Patterns\Creational\Singelton;

final class President
{
    private static $instance;

    public static function getInstance(): President
    {
        if (!self::$instance) {
            self::$instance = new President();
        }

        return self::$instance;
    }

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {

    }
}
