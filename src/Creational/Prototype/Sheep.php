<?php

declare(strict_types=1);

namespace Benedya\Patterns\Creational\Prototype;

class Sheep
{
    protected $name;
    protected $category;

    function __construct(string $name, string $category)
    {
        $this->name = $name;
        $this->category = $category;
    }

    public function setName(string $name): Sheep
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setCategory(string $category): Sheep
    {
        $this->category = $category;
        return $this;
    }

    public function getCategory(): string
    {
        return $this->category;
    }


}
