<?php

namespace Benedya\Patterns\Creational\SimpleFactory;

interface IDoor
{
    function getWidth(): float;
    function getHeight(): float;
}
