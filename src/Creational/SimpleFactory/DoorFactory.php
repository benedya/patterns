<?php

namespace Benedya\Patterns\Creational\SimpleFactory;

class DoorFactory
{
    public static function create(float $width, float $height)
    {
        return new Door($width, $height);
    }
}
