<?php

namespace Benedya\Patterns\Creational\SimpleFactory;

class Door implements IDoor
{
    protected $width;
    protected $height;

    function __construct(float $width, float $height)
    {
        $this->width = $width;
        $this->height = $height;
    }

    function getWidth(): float
    {
        return $this->width;
    }

    function getHeight(): float
    {
        return $this->height;
    }
}
