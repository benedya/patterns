<?php

namespace Benedya\Patterns\Creational\FabricMethod;

interface IStaff
{
    function askQuestion();
}
