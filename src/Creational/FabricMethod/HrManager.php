<?php

namespace Benedya\Patterns\Creational\FabricMethod;

abstract class HrManager
{
    public abstract function createInterviewer(): IStaff;

    public function takeInterview()
    {
        $interviewer = $this->createInterviewer();
        $interviewer->askQuestion();
    }
}
