<?php

namespace Benedya\Patterns\Creational\FabricMethod;

class MarketerInterviewer extends HrManager
{
    public function createInterviewer(): IStaff
    {
        return new CommunityExecutor();
    }
}
