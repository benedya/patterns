<?php

namespace Benedya\Patterns\Creational\FabricMethod;

class Developer implements IStaff
{
    public function askQuestion()
    {
        echo "\n Tell about patterns.";
    }
}
