<?php

namespace Benedya\Patterns\Creational\FabricMethod;

class CommunityExecutor implements IStaff
{
    public function askQuestion()
    {
        echo "\n Tell about society.";
    }
}
