<?php

namespace Benedya\Patterns\Creational\FabricMethod;

class DeveloperInterviewer extends HrManager
{
    public function createInterviewer(): IStaff
    {
        return new Developer();
    }
}
