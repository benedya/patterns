<?php

declare(strict_types=1);

namespace Benedya\Patterns\Creational\Builder;

class BurgerBuilder
{
    protected $cheese;
    protected $meat;
    protected $size;

    function __construct(int $size)
    {
        $this->size = $size;
    }

    public function addCheese(): BurgerBuilder
    {
        $this->cheese = true;
        return $this;
    }

    public function addMeat(): BurgerBuilder
    {
        $this->meat = true;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCheese()
    {
        return $this->cheese;
    }

    /**
     * @return mixed
     */
    public function getMeat()
    {
        return $this->meat;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    public function buildBurger(): Burger
    {
        return new Burger($this);
    }
}
