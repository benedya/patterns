<?php

declare(strict_types=1);

namespace Benedya\Patterns\Creational\Builder;

class Burger
{
    protected $cheese;
    protected $meat;
    protected $size;

    function __construct(BurgerBuilder $builder)
    {
        $this->size = $builder->getSize();
        $this->meat = $builder->getMeat();
        $this->cheese = $builder->getCheese();
    }
}
