<?php

declare(strict_types=1);

namespace Benedya\Patterns\Creational\AbstractFactory;

class IronFactory extends DoorFactory
{
    function makeDoor(): IDoor
    {
        return new IronDoor();
    }

    function makeExpert(): IDoorExpert
    {
        return new Welder();
    }
}
