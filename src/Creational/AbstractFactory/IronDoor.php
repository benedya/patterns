<?php

declare(strict_types=1);

namespace Benedya\Patterns\Creational\AbstractFactory;

class IronDoor implements IDoor
{
    public function getDescription(): string
    {
        return 'Iron door';
    }
}
