<?php

declare(strict_types=1);

namespace Benedya\Patterns\Creational\AbstractFactory;

class WoodFactory extends DoorFactory
{
    function makeDoor(): IDoor
    {
        return new WoodDor();
    }

    function makeExpert(): IDoorExpert
    {
        return new Carpenter();
    }
}
