<?php

declare(strict_types=1);

namespace Benedya\Patterns\Creational\AbstractFactory;

class WoodDor implements IDoor
{
    public function getDescription(): string
    {
        return 'Wood door';
    }
}
