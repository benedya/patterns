<?php

declare(strict_types=1);

namespace Benedya\Patterns\Creational\AbstractFactory;

class Carpenter implements IDoorExpert
{
    public function getDescription(): string
    {
        return 'I am carpenter';
    }
}
