<?php

declare(strict_types=1);

namespace Benedya\Patterns\Creational\AbstractFactory;

abstract class DoorFactory
{
    abstract function makeDoor(): IDoor;
    abstract function makeExpert(): IDoorExpert;
}
