<?php

declare(strict_types=1);

namespace Benedya\Patterns\Creational\AbstractFactory;

interface IDoorExpert
{
    function getDescription(): string;
}
