<?php

namespace Benedya\Patterns\Additional\EAV;

class Entity
{
    protected $name;
    protected $values;

    function __construct(string $name, array $values)
    {
        $this->name = $name;
        $this->values = new \SplObjectStorage();
        foreach ($values as $value) {
            $this->values->attach($value);
        }
    }

    function __toString()
    {
        $arr = [$this->name];
        foreach ($this->values as $value) {
            $arr[] = (string)$value;
        }
        return join(', ', $arr);
    }
}
