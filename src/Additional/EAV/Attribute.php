<?php

namespace Benedya\Patterns\Additional\EAV;

class Attribute
{
    protected $name;
    protected $values;

    function __construct(string $name)
    {
        $this->name = $name;
        $this->values = new \SplObjectStorage();
    }

    public function addValue(Value $value)
    {
        $this->values->attach($value);
    }

    public function getValues(): \SplObjectStorage
    {
        return $this->values;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
