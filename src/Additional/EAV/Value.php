<?php

namespace Benedya\Patterns\Additional\EAV;

class Value
{
    /** @var  Attribute */
    protected $attribute;
    protected $name;

    function __construct(Attribute $attribute, string $name)
    {
        $this->name = $name;
        $attribute->addValue($this);
        $this->attribute = $attribute;
    }

    function __toString()
    {
        return sprintf(
            "%s: %s",
            $this->attribute->getName(),
            $this->name
        );
    }
}
