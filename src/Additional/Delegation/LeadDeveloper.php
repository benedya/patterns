<?php

namespace Benedya\Patterns\Additional\Delegation;

class LeadDeveloper
{
    protected $junior;

    function __construct(JuniorDeveloper $juniorDeveloper)
    {
        $this->junior = $juniorDeveloper;
    }

    function writeCode()
    {
        $this->junior->writeBadCode();
    }
}
