<?php

namespace Benedya\Patterns\Additional\Delegation;

class JuniorDeveloper
{
    public function writeBadCode()
    {
        echo "\n write bad code";
    }
}
