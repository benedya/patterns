<?php

namespace Benedya\Patterns\Additional\Repository;

class PostRepository
{
    protected $persistence;

    function __construct(MemoryStorage $memoryStorage)
    {
        $this->persistence = $memoryStorage;
    }

    public function findById(int $id)
    {
        $data = $this->persistence->find($id);
        if (!$data) {
            throw new \OutOfRangeException(sprintf('Post not found by id %s', $id));
        }
        return Post::fromState($data);
    }

    public function save(Post $post)
    {
        $id = $this->persistence->persist([
            'title' => $post->getTitle(),
            'description' => $post->getDescription(),
        ]);
        $post->setId($id);
    }

    public function remove(Post $post)
    {
        $this->persistence->remove($post->getId());
    }
}
