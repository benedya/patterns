<?php

namespace Benedya\Patterns\Additional\Repository;

class MemoryStorage
{
    /** @var  int */
    protected $lastId;
    /** @var  string */
    protected $data = [];

    public function find(int $id)
    {
        if (!isset($this->data[$id])) {
            throw new \OutOfRangeException(sprintf('Data not found by id %s', $id));
        }
        return $this->data[$id];
    }

    public function persist(array $data): int
    {
        $this->lastId++;
        $data['id'] = $this->lastId;
        $this->data[$this->lastId] = $data;

        return $this->lastId;
    }

    public function remove($id)
    {
        if (!isset($this->data[$id])) {
            throw new \OutOfRangeException(sprintf('Data not found by id %s', $id));
        }
        unset($this->data[$id]);
    }
}
