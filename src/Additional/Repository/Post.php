<?php

namespace Benedya\Patterns\Additional\Repository;

class Post
{
    /** @var  int */
    protected $id;
    /** @var  string */
    protected $title;
    /** @var  string */
    protected $description;

    function __construct($id, string $title, string $description)
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
    }

    public static function fromState(array $data): Post
    {
        return new self(
            $data['id'] ?? null,
            $data['title'],
            $data['description']
        );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): Post
    {
        $this->id = $id;
        return $this;
    }

    public function setTitle(string $title): Post
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}
