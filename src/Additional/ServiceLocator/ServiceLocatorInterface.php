<?php

namespace Benedya\Patterns\Additional\ServiceLocator;

interface ServiceLocatorInterface
{
    function has(string $interface);
    function get(string $interface);
}
