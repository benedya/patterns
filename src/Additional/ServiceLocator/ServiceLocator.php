<?php

namespace Benedya\Patterns\Additional\ServiceLocator;

class ServiceLocator implements ServiceLocatorInterface
{
    protected $shared = [];
    protected $instantiated = [];
    protected $services = [];

    public function addService(string $class, $service, bool $shared = false)
    {
        $this->services[$class] = $service;
        $this->instantiated[$class] = $service;
        $this->shared[$class] = $shared;
    }

    public function addClass(string $class, array $args, bool $shared = false)
    {
        $this->services[$class] = $args;
        $this->shared[$class] = $shared;
    }

    function has(string $interface)
    {
        return isset($this->services[$interface]) || isset($this->instantiated[$interface]);
    }

    function get(string $interface)
    {
        if (isset($this->instantiated[$interface]) and $this->shared[$interface]) {
            return $this->instantiated[$interface];
        }
        $args = $this->services[$interface];
        $service = null;
        switch (count($args)) {
            case 0:
                $service = new $interface();
                break;
            case 1:
                $service = new $interface($args['0']);
                break;
            case 2:
                $service = new $interface($args['0'], $args['1']);
                break;
            default:
                throw new \OutOfRangeException;
        }
        if (isset($this->shared[$interface]) and $this->shared[$interface]) {
            $this->instantiated[$interface] = $service;
            echo 'added';
        }
        echo "\n\n\n created \n\n\n";

        return $service;
    }
}
