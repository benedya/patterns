<?php

namespace Benedya\Patterns\Tests\Structural\Registry;

use Benedya\Patterns\Structural\Registry\Registry;
use PHPUnit\Framework\TestCase;

class RegistryTest extends TestCase
{
    function testLogger()
    {
        $logger = new \stdClass();
        Registry::set(Registry::LOGGER, $logger);
        $savedLogger = Registry::get(Registry::LOGGER);
        $this->assertSame($logger, $savedLogger);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    function testWrongKey()
    {
        $logger = new \stdClass();
        Registry::set('test', $logger);
    }
}
