<?php

namespace Benedya\Patterns\Tests\Structural\Proxy;

use PHPUnit\Framework\TestCase;

class ProxyTest extends TestCase
{
    function testProxy()
    {
        $proxy = new \Benedya\Patterns\Structural\Proxy\RecordProxy([]);
        $proxy->name = 'benedya';
        $this->assertEquals('benedya', $proxy->name);
    }
}
