<?php

namespace Benedya\Patterns\Tests\Structural\Bridge;

use Benedya\Patterns\Structural\Bridge\About;
use Benedya\Patterns\Structural\Bridge\DarkTheme;
use PHPUnit\Framework\TestCase;

class AboutTest extends TestCase
{
    public function testGetContent()
    {
        $about = new About(
            new DarkTheme()
        );
        $this->assertRegExp('/dark/', $about->getContent());
    }
}
