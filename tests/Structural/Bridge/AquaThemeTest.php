<?php

namespace Benedya\Patterns\Tests\Structural\Bridge;

use Benedya\Patterns\Structural\Bridge\AquaTheme;
use PHPUnit\Framework\TestCase;

class AquaThemeTest extends TestCase
{
    public function testGetColor()
    {
        $aqua = new AquaTheme();
        $this->assertRegExp('/aqua/', $aqua->getColor());
    }
}
