<?php

namespace Benedya\Patterns\Tests\Structural\Bridge;

use Benedya\Patterns\Structural\Bridge\DarkTheme;
use PHPUnit\Framework\TestCase;

class DarkThemeTest extends TestCase
{
    public function testGetColor()
    {
        $theme = new DarkTheme();
        $this->assertRegExp('/dark/', $theme->getColor());
    }
}
