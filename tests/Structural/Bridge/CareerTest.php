<?php

namespace Benedya\Patterns\Tests\Structural\Bridge;

use Benedya\Patterns\Structural\Bridge\AquaTheme;
use Benedya\Patterns\Structural\Bridge\Career;
use PHPUnit\Framework\TestCase;

class CareerTest extends TestCase
{
    public function testGetContent()
    {
        $about = new Career(
            new AquaTheme()
        );
        $this->assertRegExp('/aqua/', $about->getContent());
    }
}
