<?php

namespace Benedya\Patterns\Tests\Structural\Adapter;

use Benedya\Patterns\Structural\Adapter\WildDog;
use PHPUnit\Framework\TestCase;

class WildDogTest extends TestCase
{
    public function testBark()
    {
        $lion = new WildDog();
        $this->expectOutputRegex("/wof/");
        $lion->bark();
    }
}
