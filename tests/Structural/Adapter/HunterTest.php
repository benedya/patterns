<?php

namespace Benedya\Patterns\Tests\Structural\Adapter;

use Benedya\Patterns\Structural\Adapter\AfricanLion;
use Benedya\Patterns\Structural\Adapter\AsianLion;
use Benedya\Patterns\Structural\Adapter\Hunter;
use Benedya\Patterns\Structural\Adapter\WildDog;
use Benedya\Patterns\Structural\Adapter\WildDogAdapter;
use PHPUnit\Framework\TestCase;

class HunterTest extends TestCase
{
    public function testHuntAfricanLion()
    {
        $africanLion = new AfricanLion();
        $this->expectOutputRegex("/rrr/");
        (new Hunter())->hunt($africanLion);
    }

    public function testHuntAsianLion()
    {
        $asianLion = new AsianLion();
        $this->expectOutputRegex("/urr/");
        (new Hunter())->hunt($asianLion);
    }

    public function testHuntWildDog()
    {
        $wildDog = new WildDogAdapter(new WildDog());
        $this->expectOutputRegex("/wof/");
        (new Hunter())->hunt($wildDog);
    }
}
