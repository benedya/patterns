<?php

namespace Benedya\Patterns\Tests\Structural\Adapter;

use Benedya\Patterns\Structural\Adapter\AsianLion;
use PHPUnit\Framework\TestCase;

class AsianLionTest extends TestCase
{
    public function testRoar()
    {
        $lion = new AsianLion();
        $this->expectOutputRegex("/urr/");
        $lion->roar();
    }
}
