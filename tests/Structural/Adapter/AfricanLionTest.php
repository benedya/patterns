<?php

namespace Benedya\Patterns\Tests\Structural\Adapter;

use Benedya\Patterns\Structural\Adapter\AfricanLion;
use PHPUnit\Framework\TestCase;

class AfricanLionTest extends TestCase
{
    public function testRoar()
    {
        $lion = new AfricanLion();
        $this->expectOutputRegex("/rrr/");
        $lion->roar();
    }
}
