<?php

namespace Benedya\Patterns\Tests\Structural\Adapter;

use Benedya\Patterns\Structural\Adapter\WildDog;
use Benedya\Patterns\Structural\Adapter\WildDogAdapter;
use PHPUnit\Framework\TestCase;

class WildDogAdapterTest extends TestCase
{
    public function testRoar()
    {
        $wildDogAdapter = new WildDogAdapter(
            new WildDog()
        );
        $this->expectOutputRegex("/wof/");
        $wildDogAdapter->roar();
    }
}
