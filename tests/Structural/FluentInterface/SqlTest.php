<?php

namespace Benedya\Patterns\Tests\Structural\FluentInterface;

use PHPUnit\Framework\TestCase;

class SqlTest extends TestCase
{
    function testSql()
    {
        $sql = new \Benedya\Patterns\Structural\FluentInterface\Sql();
        $sql->select(['name'])
            ->from('users', 'u')
            ->where('id = 1')
        ;
        $this->assertEquals('select name from users as u where id = 1', $sql . '');
    }

}
