<?php

namespace Benedya\Patterns\Tests\Structural\Decorator;

use Benedya\Patterns\Structural\Facade\Computer;
use Benedya\Patterns\Structural\Facade\Facade;
use PHPUnit\Framework\TestCase;

class FacadeTest extends TestCase
{
    function testGetElectricShock()
    {
        $computer = new Facade(
            new Computer()
        );
        $this->expectOutputRegex('/\n electric shock\n bip\n loading...\n ready/');
        $computer->on();
    }

    function testOff()
    {
        $computer = new Facade(
            new Computer()
        );
        $this->expectOutputRegex('/\n close all\n zzz/');
        $computer->off();
    }
}
