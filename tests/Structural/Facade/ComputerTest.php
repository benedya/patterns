<?php

namespace Benedya\Patterns\Tests\Structural\Decorator;

use Benedya\Patterns\Structural\Facade\Computer;
use PHPUnit\Framework\TestCase;

class ComputerTest extends TestCase
{
    function testGetElectricShock()
    {
        $computer = new Computer();
        $this->expectOutputRegex('/electric shock/');
        $computer->getElectricShock();
    }

    function testSayBip()
    {
        $computer = new Computer();
        $this->expectOutputRegex('/bip/');
        $computer->sayBip();
    }

    function testShowLoadingScreen()
    {
        $computer = new Computer();
        $this->expectOutputRegex('/loading/');
        $computer->showLoadingScreen();
    }

    function testReady()
    {
        $computer = new Computer();
        $this->expectOutputRegex('/ready/');
        $computer->ready();
    }

    function testCloseAll()
    {
        $computer = new Computer();
        $this->expectOutputRegex('/close all/');
        $computer->closeAll();
    }

    function testSooth()
    {
        $computer = new Computer();
        $this->expectOutputRegex('/zzz/');
        $computer->sooth();
    }
}
