<?php

namespace Benedya\Patterns\Tests\Structural\Mapper;

use Benedya\Patterns\Structural\Flyweight\GreenTea;
use Benedya\Patterns\Structural\Mapper\User;
use PHPUnit\Framework\TestCase;

class MapperTest extends TestCase
{
    function testMapper()
    {
        $adapter = new \Benedya\Patterns\Structural\Mapper\Adapter([
            1 => ['name' => 'Bohdan', 'email' => 'benedya@gmail.com']
        ]);
        $mapper = new \Benedya\Patterns\Structural\Mapper\UserMapper($adapter);
        $this->assertInstanceOf(User::class, $mapper->findById(1));
    }

    /**
     * @expectedException \Exception
     */
    function testWrongMapper()
    {
        $adapter = new \Benedya\Patterns\Structural\Mapper\Adapter([]);
        $mapper = new \Benedya\Patterns\Structural\Mapper\UserMapper($adapter);
        $mapper->findById(1);
    }
}
