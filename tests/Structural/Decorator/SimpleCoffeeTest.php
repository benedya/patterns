<?php

namespace Benedya\Patterns\Tests\Structural\Decorator;

use Benedya\Patterns\Structural\Decorator\SimpleCoffee;
use PHPUnit\Framework\TestCase;

class SimpleCoffeeTest extends TestCase
{
    function testGetCost()
    {
        $coffee = new SimpleCoffee(10);
        $this->assertEquals(10, $coffee->getCost());
    }

    function testGetDescription()
    {
        $coffee = new SimpleCoffee(10);
        $this->assertEquals('Coffee', $coffee->getDescription());
    }
}
