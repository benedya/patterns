<?php

namespace Benedya\Patterns\Tests\Structural\Decorator;

use Benedya\Patterns\Structural\Decorator\MilkCoffeeDecorator;
use Benedya\Patterns\Structural\Decorator\SimpleCoffee;
use PHPUnit\Framework\TestCase;

class MilkCoffeeDecoratorTest extends TestCase
{
    function testGetCost()
    {
        $coffee = new SimpleCoffee(10);
        $milkCoffeeDecorator = new MilkCoffeeDecorator($coffee);
        $this->assertEquals(12, $milkCoffeeDecorator->getCost());
    }

    function testGetDescription()
    {
        $coffee = new SimpleCoffee(10);
        $milkCoffeeDecorator = new MilkCoffeeDecorator($coffee);
        $this->assertRegExp('/milk/', $milkCoffeeDecorator->getDescription());
    }
}
