<?php

namespace Benedya\Patterns\Tests\Structural\DependencyInjection;

use PHPUnit\Framework\TestCase;

class DependencyInjectionTest extends TestCase
{
    function testDsn()
    {
        $configuration = new \Benedya\Patterns\Structural\DependencyInjection\DatabaseConfiguration(
            'localhost',
            'root',
            'db',
            'turbo'
        );
        $connection = new \Benedya\Patterns\Structural\DependencyInjection\DatabaseConnection($configuration);
        $this->assertEquals('localhost:db:root:turbo', $connection->getDsn());
    }
}
