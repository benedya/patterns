<?php

namespace Benedya\Patterns\Tests\Structural\Flyweight;

use Benedya\Patterns\Structural\Flyweight\GreenTea;
use PHPUnit\Framework\TestCase;

class GreenTeaTest extends TestCase
{
    function testGetType()
    {
        $tea = new GreenTea();
        $this->assertEquals('Green', $tea->getType());
    }

}
