<?php

namespace Benedya\Patterns\Tests\Structural\Flyweight;

use Benedya\Patterns\Structural\Flyweight\GreenTea;
use Benedya\Patterns\Structural\Flyweight\TeaMaker;
use PHPUnit\Framework\TestCase;

class TeaMakerTest extends TestCase
{
    function testMake()
    {
        $maker = new TeaMaker();
        $this->assertInstanceOf(GreenTea::class, $maker->make('green'));
    }

    function testCount()
    {
        $maker = new TeaMaker();
        $maker->make('green');
        $maker->make('green');
        $maker->make('red');
        $this->assertEquals(2, $maker->count());
    }
}
