<?php

namespace Benedya\Patterns\Tests\Structural\Flyweight;

use Benedya\Patterns\Structural\Flyweight\TeaMaker;
use Benedya\Patterns\Structural\Flyweight\TeaMarket;
use PHPUnit\Framework\TestCase;

class TeaMarketTest extends TestCase
{
    function testTakeOrder()
    {
        $maker = new TeaMaker();
        $market = new TeaMarket($maker);
        $market->takeOrder('green', 1);
        $market->takeOrder('green', 2);
        $market->takeOrder('black', 3);
        $this->assertEquals(2, $maker->count());
    }

    function testServe()
    {
        $maker = new TeaMaker();
        $market = new TeaMarket($maker);
        $market->takeOrder('green', 1);
        $this->expectOutputRegex("/table 1 type Green/");
        $market->serve();
    }

}
