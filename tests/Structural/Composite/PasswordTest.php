<?php

namespace Benedya\Patterns\Tests\Structural\Composite;

use Benedya\Patterns\Structural\Composite\Password;
use PHPUnit\Framework\TestCase;

class PasswordTest extends TestCase
{
    public function testRender()
    {
        $input = new Password();
        $this->assertRegExp('/type="password"/', $input->render());
    }
}
