<?php

namespace Benedya\Patterns\Tests\Structural\Composite;

use Benedya\Patterns\Structural\Composite\Input;
use PHPUnit\Framework\TestCase;

class InputTest extends TestCase
{
    public function testRender()
    {
        $input = new Input('login');
        $this->assertRegExp('/login/', $input->render());
    }
}
