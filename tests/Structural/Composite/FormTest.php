<?php

namespace Benedya\Patterns\Tests\Structural\Composite;

use Benedya\Patterns\Structural\Composite\Form;
use Benedya\Patterns\Structural\Composite\Input;
use Benedya\Patterns\Structural\Composite\Password;
use PHPUnit\Framework\TestCase;

class FormTest extends TestCase
{
    public function testAddElement()
    {
        $form = new Form();
        $form->addElement(
            new Input('login')
        );
        $this->assertRegExp('/login/', $form->render());
    }

    function testRender()
    {
        $form = new Form();
        $form->addElement(
            new Input('login')
        );
        $form->addElement(
            new Password()
        );
        $subForm = new Form();
        $subForm->addElement(new Input('family'));
        $form->addElement($subForm);

        $result = $form->render();
        $this->assertRegExp('/login/', $result);
        $this->assertRegExp('/password/', $result);
        $this->assertRegExp('/family/', $result);
    }
}
