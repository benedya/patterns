<?php

namespace Benedya\Patterns\Tests\Additional\EAV;

use Benedya\Patterns\Additional\EAV\Attribute;
use Benedya\Patterns\Additional\EAV\Entity;
use Benedya\Patterns\Additional\EAV\Value;
use PHPUnit\Framework\TestCase;;

class EAVTest extends TestCase
{
    public function testEntity()
    {
        $attr = new Attribute('color');
        $attrMemory = new Attribute('memory');

        $value = new Value($attr, 'black');
        $value2 = new Value($attr, 'blue');
        $value3 = new Value($attrMemory, '8GB');


        $entity = new Entity('pc', [$value, $value2, $value3]);
        $this->assertSame('pc, color: black, color: blue, memory: 8GB', (string)$entity);
    }
}
