<?php

namespace Benedya\Patterns\Tests\Additional\ServiceLocator;

use Benedya\Patterns\Additional\ServiceLocator\ServiceLocator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Logger\ConsoleLogger;

class ServiceLocatorTest extends TestCase
{
    /** @var  ServiceLocator */
    protected $serviceLocator;

    public function setUp()
    {
        $this->serviceLocator = new ServiceLocator();
    }

    public function testExisting()
    {
        $this->serviceLocator->addClass(ConsoleLogger::class, []);
        $this->assertTrue($this->serviceLocator->has(ConsoleLogger::class));
        $this->assertFalse($this->serviceLocator->has(self::class));
    }

    public function testInstantiated()
    {
        $this->serviceLocator->addService(self::class, new self(), true);
        $this->assertInstanceOf(self::class, $this->serviceLocator->get(self::class));
        $this->assertInstanceOf(self::class, $this->serviceLocator->get(self::class));
    }
}
