<?php

namespace Benedya\Patterns\Tests\Creational\SimpleFactory;

use Benedya\Patterns\Creational\SimpleFactory\Door;
use Benedya\Patterns\Creational\SimpleFactory\DoorFactory;
use PHPUnit\Framework\TestCase;

class DoorFactoryTest extends TestCase
{
    public function testCreate()
    {
        $this->assertInstanceOf(Door::class, DoorFactory::create(30.5, 40.5));
    }
}
