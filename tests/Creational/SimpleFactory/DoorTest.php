<?php

namespace Benedya\Patterns\Tests\Creational\SimpleFactory;

use Benedya\Patterns\Creational\SimpleFactory\Door;
use PHPUnit\Framework\TestCase;

class DoorTest extends TestCase
{
    public function testGetWidth()
    {
        $door = new Door(30.5, 40.5);
        $this->assertEquals(30.5, $door->getWidth());
    }

    public function testGetHeight()
    {
        $door = new Door(30.5, 40.5);
        $this->assertEquals(40.5, $door->getHeight());
    }
}
