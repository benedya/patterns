<?php

namespace Benedya\Patterns\Tests\Creational\FabricMethod;

use Benedya\Patterns\Creational\FabricMethod\Developer;
use PHPUnit\Framework\TestCase;

class DeveloperTest extends TestCase
{
    public function testAskQuestion()
    {
        $staff = new Developer();
        $this->assertEquals(null, $staff->askQuestion());
    }
}
