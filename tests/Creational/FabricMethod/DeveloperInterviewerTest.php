<?php

namespace Benedya\Patterns\Tests\Creational\FabricMethod;

use Benedya\Patterns\Creational\FabricMethod\Developer;
use Benedya\Patterns\Creational\FabricMethod\DeveloperInterviewer;
use PHPUnit\Framework\TestCase;

class DeveloperInterviewerTest extends TestCase
{
    public function testCreateInterviewer()
    {
        $interviewer = new DeveloperInterviewer();
        $this->assertInstanceOf(Developer::class, $interviewer->createInterviewer());
    }


    public function testTakeInterview()
    {
        $interviewer = new DeveloperInterviewer();
        $this->expectOutputRegex("/patterns/");
        $interviewer->createInterviewer()->askQuestion();
    }
}
