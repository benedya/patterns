<?php

namespace Benedya\Patterns\Tests\Creational\FabricMethod;

use Benedya\Patterns\Creational\FabricMethod\CommunityExecutor;
use Benedya\Patterns\Creational\FabricMethod\MarketerInterviewer;
use PHPUnit\Framework\TestCase;

class MarketerInterviewerTest extends TestCase
{
    public function testCreateInterviewer()
    {
        $interviewer = new MarketerInterviewer();
        $this->assertInstanceOf(CommunityExecutor::class, $interviewer->createInterviewer());
    }

    public function testTakeInterview()
    {
        $interviewer = new MarketerInterviewer();
        $this->expectOutputRegex("/society/");
        $interviewer->createInterviewer()->askQuestion();
    }
}
