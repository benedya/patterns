<?php

namespace Benedya\Patterns\Tests\Creational\FabricMethod;

use Benedya\Patterns\Creational\FabricMethod\CommunityExecutor;
use PHPUnit\Framework\TestCase;

class CommunityExecutorTest extends TestCase
{
    public function testAskQuestion()
    {
        $staff = new CommunityExecutor();
        $this->assertEquals(null, $staff->askQuestion());
    }
}
