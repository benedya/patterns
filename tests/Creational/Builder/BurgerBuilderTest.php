<?php

namespace Benedya\Patterns\Tests\Creational\Builder;

use Benedya\Patterns\Creational\Builder\Burger;
use Benedya\Patterns\Creational\Builder\BurgerBuilder;
use PHPUnit\Framework\TestCase;

class BurgerBuilderTest extends TestCase
{
    public function testAddCheese()
    {
        $builder = new BurgerBuilder(10);
        $builder->addCheese();
        $this->assertEquals(true, $builder->getCheese());
    }

    public function testAddMeat()
    {
        $builder = new BurgerBuilder(10);
        $builder->addMeat();
        $this->assertEquals(true, $builder->getMeat());
    }

    public function testGetCheese()
    {
        $builder = new BurgerBuilder(10);
        $builder->addCheese();
        $this->assertEquals(true, $builder->getCheese());
    }

    public function testGetMeat()
    {
        $builder = new BurgerBuilder(10);
        $builder->addMeat();
        $this->assertEquals(true, $builder->getMeat());
    }

    public function testGetSize()
    {
        $builder = new BurgerBuilder(10);
        $this->assertEquals(10, $builder->getSize());
    }

    public function testBuildBurger()
    {
        $builder = new BurgerBuilder(10);
        $this->assertInstanceOf(Burger::class, $builder->buildBurger());
    }
}
