<?php

namespace Benedya\Patterns\Tests\Creational\Prototype;

use Benedya\Patterns\Creational\Prototype\Sheep;
use PHPUnit\Framework\TestCase;

class SheepTest extends TestCase
{
    public function testPrototype()
    {
        $prototype = new Sheep('Sofa', 'Mountain Sheep');
        $this->assertInstanceOf(Sheep::class, $prototype);
        $prototype2 = clone $prototype;
        $prototype2->setName('Inga');
        $this->assertInstanceOf(Sheep::class, $prototype2);

    }
}
