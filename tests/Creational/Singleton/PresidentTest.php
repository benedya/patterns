<?php

namespace Benedya\Patterns\Tests\Creational\Singleton;

use Benedya\Patterns\Creational\Singelton\President;
use PHPUnit\Framework\TestCase;

class PresidentTest extends TestCase
{
    public function testSingleton()
    {
        $singleton1 = President::getInstance();
        $singleton2 = President::getInstance();
        $this->assertInstanceOf(President::class, $singleton1);
        $this->assertSame($singleton1, $singleton2);

    }
}
