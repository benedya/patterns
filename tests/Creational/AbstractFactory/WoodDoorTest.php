<?php

namespace Benedya\Patterns\Tests\Creational\AbstractFactory;

use Benedya\Patterns\Creational\AbstractFactory\WoodDor;
use PHPUnit\Framework\TestCase;

class WoodDoorTest extends TestCase
{
    public function testGetDescription()
    {
        $door = new WoodDor();
        $this->assertRegExp('/Wood door/', $door->getDescription());
    }
}
