<?php

namespace Benedya\Patterns\Tests\Creational\AbstractFactory;

use Benedya\Patterns\Creational\AbstractFactory\Carpenter;
use PHPUnit\Framework\TestCase;

class CarpenterTest extends TestCase
{
    public function testGetDescription()
    {
        $carpenter = new Carpenter();
        $this->assertRegExp('/carpenter/', $carpenter->getDescription());
    }
}
