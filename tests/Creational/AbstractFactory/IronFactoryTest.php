<?php

namespace Benedya\Patterns\Tests\Creational\AbstractFactory;

use Benedya\Patterns\Creational\AbstractFactory\IronDoor;
use Benedya\Patterns\Creational\AbstractFactory\IronFactory;
use Benedya\Patterns\Creational\AbstractFactory\Welder;
use PHPUnit\Framework\TestCase;

class IronFactoryTest extends TestCase
{
    public function testMakeDoor()
    {
        $factory = new IronFactory();
        $this->assertInstanceOf(IronDoor::class, $factory->makeDoor());
        $this->assertInstanceOf(Welder::class, $factory->makeExpert());
    }
}
