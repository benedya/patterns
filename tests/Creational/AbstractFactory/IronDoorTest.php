<?php

namespace Benedya\Patterns\Tests\Creational\AbstractFactory;

use Benedya\Patterns\Creational\AbstractFactory\IronDoor;
use PHPUnit\Framework\TestCase;

class IronDoorTest extends TestCase
{
    public function testGetDescription()
    {
        $door = new IronDoor();
        $this->assertRegExp('/Iron door/', $door->getDescription());
    }
}
