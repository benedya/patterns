<?php

namespace Benedya\Patterns\Tests\Creational\AbstractFactory;

use Benedya\Patterns\Creational\AbstractFactory\Carpenter;
use Benedya\Patterns\Creational\AbstractFactory\WoodDor;
use Benedya\Patterns\Creational\AbstractFactory\WoodFactory;
use PHPUnit\Framework\TestCase;

class WoodFactoryTest extends TestCase
{
    public function testMakeDoor()
    {
        $factory = new WoodFactory();
        $this->assertInstanceOf(WoodDor::class, $factory->makeDoor());
        $this->assertInstanceOf(Carpenter::class, $factory->makeExpert());
    }
}
