<?php

namespace Benedya\Patterns\Tests\Creational\AbstractFactory;

use Benedya\Patterns\Creational\AbstractFactory\Welder;
use PHPUnit\Framework\TestCase;

class WelderTest extends TestCase
{
    public function testGetDescription()
    {
        $carpenter = new Welder();
        $this->assertRegExp('/I am welder/', $carpenter->getDescription());
    }
}
